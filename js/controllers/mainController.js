var app = angular.module('devilApp', []);
var app = angular.module("devilApp", ["ngRoute" , "ngSanitize" , 'ngAnimate', 'ngCookies','angularFileUpload',"ckeditor"]);
app.config(function($routeProvider , $locationProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider
    .when("/", {
        templateUrl : "pages/main.html",
        controller : "mainController"
    })
    .when("/devil", {
        templateUrl : "pages/admin.html",
        controller : "admController",
        controllerAs: 'vm'
    })
    .when("/devil/texts", {
        templateUrl : "pages/adminTexts.html",
        controller : "admController",
        controllerAs: 'vm'
    })
    .when("/devil/slider", {
        templateUrl : "pages/adminSlider.html",
        controller : "admController",
        controllerAs: 'vm'
    })
    .when("/devil/gallery", {
        templateUrl : "pages/adminGallery.html",
        controller : "admController",
        controllerAs: 'vm'
    })
    .when("/devil/copy", {
        templateUrl : "pages/adminCopy.html",
        controller : "admController",
        controllerAs: 'vm'
    })
    .when("/devil/edit", {
        templateUrl : "pages/adminEdit.html",
        controller : "admController",
        controllerAs: 'vm'
    })
    .when("/login", {
        templateUrl : "pages/login.html",
        controller : "loginController",
        controllerAs: 'vm'
    })
    .otherwise({"redirectTo":"/"});
});

app.controller('mainController', function($scope, $http, $rootScope, $document, $timeout) {
    $scope.pageClass = 'main-page';
    $http.get(String("/config/bd.php"))
        .then(function(response) {
            $scope.items = response.data;
    });
    $scope.submitMainForm = function (all) {
        var nameArea = angular.element('form[name="mainForm"] input[name="name"]');
        var mailArea = angular.element('form[name="mainForm"] input[name="mail"]');
        var phoneArea = angular.element('form[name="mainForm"] input[name="phone"]');
        if (nameArea.val() == '') {
            nameArea.parent().addClass('error')
            if (!angular.element('form[name="mainForm"] .nameError').length) {
                nameArea.parent().append('<div class="error nameError">Имя обязательно для заполнения</div>');
            }
            var name = '';
        }else{
            angular.element('form[name="mainForm"] .nameError').remove();
            var name = $scope.name;
        }
        if (mailArea.val() == '') {
            mailArea.parent().addClass('error')
            if (!angular.element('form[name="mainForm"] .mailError').length) {
                mailArea.parent().append('<div class="error mailError">Mail обязательно для заполнения</div>');
            }
            var mail = '';
        }else{
            angular.element('form[name="mainForm"] .mailError').remove();
            var mail = $scope.mail;
        }
        if (phoneArea.val() == '') {
            phoneArea.parent().addClass('error')
            if (!angular.element('form[name="mainForm"] .phoneError').length) {
                phoneArea.parent().append('<div class="error phoneError">Телефон обязательно для заполнения</div>');
            }
            var phone = '';
        }else{
            angular.element('form[name="mainForm"] .phoneError').remove();
            var phone = $scope.phone;
        }
        if (phone != '' && mail != '' && name != '') {
            var data = {phone:phone,name:name,mail:mail,mailSend:$rootScope.main.mainMailSend}
            $http({
                method: 'POST', 
                url: "/form-handler.php",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},  
                data: data
            }).then(function(response) {
                if (response.data.status == "success") {
                    if ($rootScope.main.yaCounterTarget && $rootScope.main.yaCounterTarget != '' && $rootScope.main.mainYandexId != '') {
                        mainFormTarget();
                    }
                    $rootScope.success = response.data.message;
                    $timeout(function(){
                        $rootScope.success = "";
                    }, 1500);
                }
            });
        }
        return false;
    }
})
.directive("owlCarousel", function() {
    return {
        restrict: 'E',
        transclude: false,
        link: function (scope) {
            scope.initCarousel = function(element) {
              // provide any default options you want
                var defaultOptions = {
                };
                var customOptions = scope.$eval($(element).attr('data-options'));
                // combine the two options objects
                for(var key in customOptions) {
                    defaultOptions[key] = customOptions[key];
                }
                $(element).owlCarousel(defaultOptions);
            };
        }
    };
})
.directive('owlCarouselItem', [function() {
    return {
        restrict: 'A',
        transclude: false,
        link: function(scope, element) {
          // wait for the last item in the ng-repeat then call init
            if(scope.$last) {
                scope.initCarousel(element.parent());
            }
        }
    };
}]);
app.controller('admController', function($scope,$rootScope, $http, FileUploader, $timeout) {
    function getAdminInfo () {
        $http.get(String("/config/bd.php?page=adminPassEdit&admin="+$rootScope.globals.currentUser.username))
            .then(function(response) {
                $scope.adminLogin = response.data.name;
                $scope.adminId = response.data.id;
                $scope.adminP = response.data.pass;
                // console.log(response)
        });
    }
    getAdminInfo();
    $rootScope.success = "";
    $scope.leftSiteBar = function (siteBar) {
        $rootScope.siteBar = siteBar;
    }
    function imageGet () {
        $http.get(String("/config/bd.php/?page=adminSlider"))
            .then(function(response) {
                $scope.items = response.data;
        });
    }
    $scope.removeImage = function (itemId) {
        $http.get(String("/config/bd.php/?page=adminRemoveSlide&id="+itemId))
            .then(function(response) {
                imageGet();
                $rootScope.success = "Готово";
                $timeout(function(){
                    $rootScope.success = "";
                }, 1500);
        });
    }
    $scope.removeGalleryImage = function (itemId) {
        $http.get(String("/config/bd.php/?page=adminRemoveGallery&id="+itemId))
            .then(function(response) {
                imageGet();
                $rootScope.success = "Готово";
                $timeout(function(){
                    $rootScope.success = "";
                }, 1500);
        });
    }
    $scope.editImage = function (itemId,itemTite,itemName,itemlinkText,itemlinkUrl,newsliderName) {
        if (!itemlinkText){
            itemlinkText = '';
        }
        $http.get(String("/config/bd.php/?page=adminEditSlide&id="+itemId+"&name="+itemName+"&linkUrl="+itemlinkUrl+"&linkText="+itemlinkText+"&slider="+newsliderName+"&slideTitle="+itemTite))
            .then(function(response) {
                imageGet();
                $rootScope.success = "Готово";
                $timeout(function(){
                    $rootScope.success = "";
                }, 1500);
        });
    }
    $scope.editGalleryImage = function (id, imageTitle, imageLink, galleryName) {
        if (!imageLink) {
            imageLink = '';
        }
        $http.get(String("/config/bd.php/?page=adminEditGallery&id="+id+"&imageTitle="+imageTitle+"&imageLink="+imageLink+"&galleryName="+galleryName))
            .then(function(response) {
                imageGet();
                $rootScope.success = "Готово";
                $timeout(function(){
                    $rootScope.success = "";
                }, 1500);
        });
    }
    function copyGet () {
        $http.get(String("/config/bd.php/?page=adminCopy"))
            .then(function(response) {
                $scope.textCopy = response.data.copyrights[0].copyrights;
                $scope.textCopyId = response.data.copyrights[0].id;
                $scope.copyText = response.data.copyrights[0].copyrights;
        });
    }
    function getTexts () {
        $http.get(String("/config/bd.php/?page=adminCopy"))
            .then(function(response) {
                $scope.landingText = response.data.blocks;
        });
    }
    $scope.submitTextEditForm = function () {
        var text_1 = $scope.formTextEdit.newLandingText_0.$viewValue;
        var text_2 = $scope.formTextEdit.newLandingText_1.$viewValue;
        var text_3 = $scope.formTextEdit.newLandingText_2.$viewValue;
        var text_4 = $scope.formTextEdit.newLandingText_3.$viewValue;
        var data = {text1:text_1,text2:text_2,text3:text_3,text4:text_4}
        $http({
            method: 'POST', 
            url: "/config/bd.php/?page=adminEditTextes",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},  
            data: data
        }).then(function(response) {
                console.log(response)
                $rootScope.success = "Готово";
                $timeout(function(){
                    $rootScope.success = "";
                }, 1500);
        });
        return false;
    }
    $scope.submitAdminForm = function (data) {
        var newName = data.admName.$viewValue;
        var id = data.admId.$viewValue;
        var newPass = data.admNewPass.$viewValue;
        var pass = data.admPass.$viewValue;
        console.log($rootScope.success)
        if (pass != $scope.adminP) {
            $rootScope.success = "Неверный пароль";
        }else if (pass == $scope.adminP && newPass.length < 6) {
            $rootScope.success = "Пароль "+newPass+" слишком короткий";
        }else{
            $http.get(String("/config/bd.php?page=adminPassUpdate&id="+id+"&newName="+newName+"&pass="+newPass))
                .then(function(response) {
                    $rootScope.success = "Готово";
                    $timeout(function(){
                        $rootScope.success = "";
                    }, 1500);
                    getAdminInfo();
            });
        }
    }
    $scope.successHide = function(){
        $rootScope.success = '';
    }
    imageGet();
    copyGet ();
    getTexts ();
    
    $scope.editCopygiht = function(copyText,copyId){
        if (copyText != '' && copyText != undefined) {
            $http.get(String("/config/bd.php/?page=adminEditCopy&copyId="+copyId+"&copyText="+copyText))
                .then(function(response) {
                    copyGet();
                    $rootScope.success = "Готово";
                    $timeout(function(){
                        $rootScope.success = "";
                    }, 1500);
            });
        }
    }
    $scope.MainAdmin = function(font,menuType,galleryCountLine,phone,mail,mailSend,mainTitle,mainYandexId,mainYandexTarget){
        if (!manuType){
            var manuType = 'normal';
        }
        if (!galleryCountLine){
            var manuType = '4';
        }
        $http.get(String("/config/bd.php/?page=adminMainEdit&font="+font+"&galleryCountLine="+galleryCountLine+"&menuType="+menuType+"&phone="+phone+"&mail="+mail+"&mailSend="+mailSend+"&mainTitle="+mainTitle+"&yaId="+mainYandexId+"&yaTarget="+mainYandexTarget))
            .then(function(response) {
                $rootScope.success = "Готово";
                $timeout(function(){
                    $rootScope.success = "";
                }, 1500);
        });
        console.log(galleryCountLine)
    }
    $scope.options = {
        language: 'ru',
        allowedContent: true,
        entities: false
    };
    var uploader = $scope.uploader = new FileUploader({
        url: 'upload.php',
        removeAfterUpload: true
    });
    // FILTERS
    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    var galleryUploader = $scope.galleryUploader = new FileUploader({
        url: 'upload.php?type=gallery',
        removeAfterUpload: true
    });
    // FILTERS
    galleryUploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    // CALLBACKS
    // uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
    //     console.info('onWhenAddingFileFailed', item, filter, options);
    // };
    // uploader.onAfterAddingFile = function(fileItem) {
    //     console.info('onAfterAddingFile', fileItem);
    // };
    // uploader.onAfterAddingAll = function(addedFileItems) {
    //     console.info('onAfterAddingAll', addedFileItems);
    // };
    // uploader.onBeforeUploadItem = function(item) {
    //     console.info('onBeforeUploadItem', item);
    // };
    // uploader.onProgressItem = function(fileItem, progress) {
    //     console.info('onProgressItem', fileItem, progress);
    // };
    // uploader.onProgressAll = function(progress) {
    //     console.info('onProgressAll', progress);
    // };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        // console.info('onSuccessItem', fileItem, response, status, headers);
        console.log($scope.uploader);
        imageGet();
    };
    galleryUploader.onSuccessItem = function(fileItem, response, status, headers) {
        // console.info('onSuccessItem', fileItem, response, status, headers);
        console.log($scope.galleryUploader);
        imageGet();
    };
    // uploader.onErrorItem = function(fileItem, response, status, headers) {
    //     console.info('onErrorItem', fileItem, response, status, headers);
    // };
    // uploader.onCancelItem = function(fileItem, response, status, headers) {
    //     console.info('onCancelItem', fileItem, response, status, headers);
    // };
    // uploader.onCompleteItem = function(fileItem, response, status, headers) {
    //     console.info('onCompleteItem', fileItem, response, status, headers);
    // };
    // uploader.onCompleteAll = function() {
    //     console.info('onCompleteAll');
    // };
    // console.info('uploader', uploader);
})
.directive('ngThumb', ['$window', function($window) {
    var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function(item) {
            return angular.isObject(item) && item instanceof $window.File;
        },
        isImage: function(file) {
            var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    };

    return {
        restrict: 'A',
        template: '<canvas/>',
        link: function(scope, element, attributes) {
            if (!helper.support) return;

            var params = scope.$eval(attributes.ngThumb);

            if (!helper.isFile(params.file)) return;
            if (!helper.isImage(params.file)) return;

            var canvas = element.find('canvas');
            var reader = new FileReader();

            reader.onload = onLoadFile;
            reader.readAsDataURL(params.file);

            function onLoadFile(event) {
                var img = new Image();
                img.onload = onLoadImage;
                img.src = event.target.result;
            }

            function onLoadImage() {
                var width = params.width || this.width / this.height * params.height;
                var height = params.height || this.height / this.width * params.width;
                canvas.attr({ width: width, height: height });
                canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
            }
        }
    };
}])
.directive('ckEditor', function ($rootScope) {
  return {
    require: '?ngModel',
    link: function (scope, elm, attr, ngModel) {
      var ck = CKEDITOR.replace(elm[0]);
      if (!ngModel) return;
      ck.on('instanceReady', function () {
        ck.setData(ngModel.$viewValue);
      });
      function updateModel() {
        scope.$apply(function () {
          ngModel.$setViewValue(ck.getData());
        });
      }
      ck.on('change', updateModel);
      ck.on('key', updateModel);
      ck.on('dataReady', updateModel);

      ngModel.$render = function (value) {
        ck.setData(ngModel.$viewValue);
      };
    }
  }
})
.directive('footerCopy', function ($rootScope) {
    // if ($rootScope.ctyle == 'a.') {
    //     var copy = '<div class="copy"><div class="copyrights" ng-bind-html="copy.copyrights">"adminko" создано по хотелке mr.barabashka <a href="http://bmdev.ru" target="_blank">dont click this</a></div></div>';      
    // }else{
    //     var copy = '<div class="copy" ng-controller="mainController"><div class="copyrights" ng-repeat="copy in items.copyrights" ng-if="$index == 0" ng-bind-html="copy.copyrights">{{copy.copyrights}}</div></div>';
    // }
    // return {
    //     templateUrl:''
    // };
}).directive('ngZoom', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      //Will watch for changes on the attribute
      attrs.$observe('zoomImage',function(){
        linkElevateZoom();
      })
      
      function linkElevateZoom(){
        //Check if its not empty
        if (!attrs.zoomImage) return;
        element.attr('data-zoom-image',attrs.zoomImage);
        $(element).elevateZoom(
            {
                tint:true,
                tintColour:'#F90',
                tintOpacity:0.5,
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 500,
                lensFadeIn: 500,
                lensFadeOut: 500,
                easing : true,
                zoomWindowPosition: 6
            }
        );
      }
      // .elevateZoom({tint:true, tintColour:'#F90', tintOpacity:0.5})
      linkElevateZoom();

    }
  };
});
app.controller('loginController', function($scope, $location, AuthenticationService, $rootScope, $timeout) {
        var vm = this;

        vm.login = login;

        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();

        function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials(vm.username, vm.password);
                    $rootScope.success = "Вы успешно авторизованы!";
                        $timeout(function(){
                            $rootScope.success = "";
                            $location.path('/devil');
                        }, 1500);
                } else {
                    vm.dataLoading = false;
                    $rootScope.success = "Неверный логин и/или пароль";
                    $timeout(function(){
                        $rootScope.success = "";
                    }, 1500);
                    // FlashService.Error(response.message);
                }
            });
        };
});
// app.run.$inject = ['$rootScope', '$location', '$cookies', '$http'];
app.run(function ($rootScope, $timeout, $window, $cookies, $location, $http, $window) {
    $rootScope.isReady = false;
    $rootScope.main = [];
    $http.get(String("/config/bd.php/?page=adminMain"))
        .then(function(response) {
            $rootScope.main.mainPhone = response.data.mainPhone;
            $rootScope.main.mainMail = response.data.mainMail;
            $rootScope.main.mainMailSend = response.data.mainMailSend;
            $rootScope.main.mainTitle = response.data.mainTitle;
            $rootScope.main.menuType = response.data.menuType;
            $rootScope.main.galleryCountLine = response.data.galleryCountLine;
            $rootScope.main.font = response.data.font;
    });
    $rootScope.ctyle = '';
    $rootScope.globals = $cookies.getObject('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
    }
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        $http.get(String("/config/bd.php/?page=adminMain"))
            .then(function(response) {
                $rootScope.main.mainPhone = response.data.mainPhone;
                $rootScope.main.mainMail = response.data.mainMail;
                $rootScope.main.mainMailSend = response.data.mainMailSend;
                $rootScope.main.mainTitle = response.data.mainTitle;
                $rootScope.main.mainYandexId = response.data.yaCounterId;
                $rootScope.main.yaCounterTarget = response.data.yaCounterTarget;
        });
        if ($location.path().indexOf('/devil') != -1){
            restrictedPage = true;
            $rootScope.ctyle = 'a.';
        }else{
            restrictedPage = false;
            $rootScope.ctyle = '';
        }
        var loggedIn = $rootScope.globals.currentUser;
        if (restrictedPage && !loggedIn) {
            $location.path('/login');
        }
        $rootScope.isReady = false;
    });

    $rootScope.$on('$viewContentLoaded', function(){
        (function (){
            $timeout(function(){
                $('.menu-block ul').removeClass('active');
                $('.menu-block ul a[href="'+$location.path()+'"]').parent().addClass('active');
                ui.paralax();
            }, 300);
            $('[name="phone"]').mask("+7 (999) 999-99-99");
            if (!$('#backTop').length) {
                $('body').append('<div id="backTop" class="baclTop" onclick="$(\'body , html\').animate({ scrollTop: 0 }, 1100);">Up</div>')
            }
            window.onscroll = function() {
                if (window.pageYOffset >= 300) {
                    $('#backTop').css({
                        'display':'block'
                    })
                }else{
                    $('#backTop').css({
                        'display':'none'
                    })
                }
            };
            $timeout(function(){
                $rootScope.isReady = true;
            }, 1000);
            BrowserDetect.init();
            if (BrowserDetect.browser == 'Chrome') {
                $rootScope.main.browser = 'ok';
            }
            $(window).resize(function(){
                if ($(window).width() <= 1000) {
                    $rootScope.siteBar = true;
                    $('.adm-page').addClass('siteBarHide')
                    $('#menu-toggle').removeClass('open')
                }else{
                    $('.adm-page').removeClass('siteBarHide')
                    $('#menu-toggle').addClass('open')
                    $rootScope.siteBar = false;
                }
            })
            ui.init();
            $(window).resize();
        })($)
    });
});
