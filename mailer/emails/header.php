<?
global $mailHeader;
$mailHeader = '<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" style="table-layout:fixed;border-collapse: collapse;width: 100%;min-width:700px; background-color: #e6eaf5; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: arial, Times, serif;background-image:url(http://benefitmarketing.ru/email-img/app-mail/bg.png);background-position:center;">
        <tbody>
            <tr>
                <td valign="top">
                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="table-layout:fixed;border-collapse: collapse;margin:0 auto;padding-top:20px;margin-bottom:5px;background-color:#fff;">
                        <tbody>
                            <tr>
                                <td style="border-bottom: 1px solid #e0e5ee;margin-bottom:4px;width: 700px;">
                                    <table border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth" style="width:50%;text-align: left;">
                                        <tbody>
                                            <tr>
                                                <td class="center" style="">
                                                    <p style="font-size: 18px; color:#307ab6;padding-left:30px;margin-bottom: 4px;">Здравствуйте!</p>
                                                    <p style="font-size: 16px; padding-left:30px;margin-top: 4px;">Вам поступила новая заявка:</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth" style="table-layout:fixed;width:50%;text-align: right;">
                                        <tbody>
                                            <tr>
                                                <td class="center" style="font-size:12px;">
                                                    <!-- текст письма - дата и время отправки -->
                                                    <p style="margin-top:12px;margin-bottom:0;padding-right:30px;">Дата и время:</p>
                                                    <p style="font-size: 13px;color: #595858;margin-top:20px;padding-right:30px;"><span style="vertical-align:middle;"><img src="http://benefitmarketing.ru/email-img/app-mail/calendar.jpg" style="display: inline-block;vertical-align: middle;margin-top: -10px;margin-right: 10px;">{currentDay}</span>&nbsp;<span style="vertical-align:middle;"><img src="http://benefitmarketing.ru/email-img/app-mail/clock.jpg" alt="" title="" style="display: inline-block;vertical-align: middle;margin-top: -10px;margin-right: 10px; margin-left:6px;">{currentTime}</span></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>';
?>