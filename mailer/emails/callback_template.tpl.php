{MAIL_HEADER}
<tr>
                                <td>
                                    <table width="700px" align="center" cellpadding="0" cellspacing="0" style="table-layout:fixed;border-collapse: collapse;border-right: 1px solid #e0e5ee;color:#9ca9ba;-webkit-font-smoothing: antialiased;font-family: arial, Times, serif;background-color:#fff">
                                        <tbody>
                                            <tr border-collapse="collapse" style="width:700px;height: 50px;padding-bottom: 10px; padding-top: 10px;border:1px solid #eef1f8;background-color: #ebeef3;border-radius: 5px;">
                                                <table width="700px" align="center" cellpadding="0" cellspacing="0" style="table-layout:fixed;border-collapse: collapse;border-right: 1px solid #e0e5ee;color:#9ca9ba;-webkit-font-smoothing: antialiased;font-family: arial, Times, serif;background-color:#ebeef3">
                                                    <tr style="height: 50px;border:1px solid #eef1f8;background-color: #ebeef3;">
                                                        <td border-collapse="collapse" style="width:196px;">
                                                            <span style="padding-left: 20px;">Проект</span>
                                                        </td>
                                                        <td border-collapse="collapse" style="width:250px;">
                                                            <span style="padding-left: 20px;color:#317bb7;font-size:14px;font-weight: bold;">{siteName}</span>
                                                        </td>
                                                        <td border-collapse="collapse" style="width:250px;">
                                                            <span style="padding-left: 20px; color:#9ca9ba;font-size:14px;font-weight: bold;">URL </span>
                                                            <span><a href="{siteUrl}" target="_blank" style="font-size: 14px;text-decoration:none;color:#000;font-weight: bold;">{siteUrl}</a></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </tr>
                                            <tr style="width:700px;height: 50px;background-color: #fff;table-layout:fixed;display: table;">
                                                <td style="width: 50%;">
                                                    <span style="padding-left: 20px;">Форма</span>
                                                </td>
                                                <td colspan="2" style="width: 50%;padding-bottom: 10px; padding-top: 10px;padding-right: 20px;">
                                                    <span style="color:#000;font-size:14px;font-weight: bold;padding-left: 20px;display: inline-block;">{formName}</span>
                                                </td>
                                            </tr>
                                            <tr style="width:700px;height: 50px;background-color: #ebeef3;table-layout:fixed;display: table;">
                                                <td style="width: 50%;">
                                                    <span style="padding-left: 20px;">Имя</span>
                                                </td>
                                                <td colspan="2" style="width: 50%;padding-bottom: 10px; padding-top: 10px;padding-right: 20px;">
                                                    <span style="color:#000;font-size:14px;font-weight: bold;padding-left: 20px;display: inline-block;">{name}</span>
                                                </td>
                                            </tr>
                                            <tr style="width:700px;height: 50px;background-color: #fff;table-layout:fixed;display: table;">
                                                <td style="width: 50%;">
                                                    <span style="padding-left: 20px;">Телефон</span>
                                                </td>
                                                <td colspan="2" style="width: 50%;padding-bottom: 10px; padding-top: 10px;padding-right: 20px;">
                                                    <span style="color:#000;font-size:14px;font-weight: bold;padding-left: 20px;display: inline-block;">{phone}</span>
                                                </td>
                                            </tr>
                                            <tr style="width:700px;height: 50px;background-color: #ebeef3;table-layout:fixed;display: table;">
                                                <td style="width: 50%;">
                                                    <span style="padding-left: 20px;">E-mail</span>
                                                </td>
                                                <td colspan="2" style="width: 50%;padding-bottom: 10px; padding-top: 10px;padding-right: 20px;">
                                                    <span style="color:#000;font-size:14px;font-weight: bold;padding-left: 20px;display: inline-block;">{mail}</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
{MAIL_FOOTER}
