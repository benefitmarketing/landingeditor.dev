<?
 global $mailFooter;
 $mailFooter = 
                '<tr>
                                <td>
                                <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="table-layout:fixed;border-collapse: collapse;margin:0 auto;padding-top:20px;margin-top:0;background-color:#fff;">
                        <tbody>
                            <tr style="background-repeat-y:no-repeat;background-image: url(http://benefitmarketing.ru/email-img/app-mail/head-bg.jpg)">
                                <td style="width: 700px;">
                                    <table border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth" style="width: 350px; background: transparent; width:50%; text-align:left;">
                                        <tbody>
                                            <tr>
                                                <td class="center">
                                                    <a href="http://benefitmarketing.ru/" style="color:#fff;text-decoration:none;" target="_blank">
                                                        <p style="font-size:20px;padding-left:30px;margin-top:12px;margin-bottom: 14px;">BENEFIT MARKETING</p>
                                                        <p style="font-size: 12px;padding-left:30px;margin-top:0;margin-bottom: 0px;padding-bottom:7px;">Вывод Вашего бизнеса онлайн!</p>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" align="rigth" class="deviceWidth" style="width: 350px; table-layout:fixed;background: transparent;width:50%; text-align:right;">
                                        <tbody>
                                            <tr>
                                                <td class="center" style="font-size:15px;margin-top: 18px;">
                                                    <p style="padding-right:30px;margin-bottom: 0;margin-top: 0;padding-top: 12px;">
                                                        <a href="tel:+ 7 (863) 310-01-35" style="color: #fff;text-decoration:none;">+ 7 (863) 310-01-35</a>
                                                    </p>
                                                    <p style="padding-right:30px;margin-bottom: 0;margin-top: 0;padding-top: 19px;">
                                                        <a href="http://benefitmarketing.ru/" style="color:#fff;text-decoration:none;" target="_blank">benefitmarketing.ru</a>
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                            </table>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="width:100%;max-width:700px;margin:0 auto;margin-top:30px;margin-bottom:50px;background:transparent;border: 1px solid #ededed;">
                        <tbody>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" class="deviceWidth" style="width:33%;background-color:#fff;border-right: 1px solid #e0e5ee;float:left;">
                                        <tr>
                                            <td style="padding:0;">
                                                <p style="line-height:25px;font-size:20px;padding-left:30px;margin-top:40px;">
                                                    Хотите<br>
                                                    <span style="font-size:26px;color:#3486c0;text-transform:uppercase;">не терять</span><br>
                                                    клиентов и<br>
                                                    <span style="font-size:26px;color:#3486c0;text-transform:uppercase;">получать</span><br>
                                                    заявки с сайта<br>
                                                    прямо в CRM?
                                                </p>
                                                <p style="line-height:20px;padding-left:30px;padding-right:30px;padding-top:30px;padding-bottom:28px;margin-bottom: 13px;">
                                                    <a href="http://crmbm.ru/" title="" style="display:block;padding-left:0;padding-top:12px;padding-bottom:12px;padding-right:0;background:url(http://benefitmarketing.ru/email-img/app-mail/head-bg.jpg) repeat-x;color:#fff;text-align:center;width:100%;text-decoration:none;font-size:16px;">Узнать как!</a>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" class="deviceWidth" style="width:67%;float:left;">
                                        <tr>
                                            <td style="padding:0;">
                                                <img src="http://benefitmarketing.ru/email-img/app-mail/mail-right.jpg" alt="" title="" style="width:100%;display:block;margin:0;padding:0;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>';
?>