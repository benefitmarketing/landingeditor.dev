<?php

namespace DigitalHammer\LpForms;

use \PHPMailer as PHPMailer;

class Mailer
{
    private $subject;
    private $message;

    private $mailFrom;
    private $nameFrom = '';
    private $mailTo = [];
    private $mail;
    private $isSmtp = false;
    private $config;

    public function __construct($from, $mailTo, $config = [])
    {
        if (is_array($from))
        {
            $this->mailFrom = $from[0];
            $this->nameFrom = $from[1];
        } else {
            $this->mailFrom = $from;
        }
        if (is_array($mailTo))
        {
            $this->mailTo[0] = $mailTo[0];
            $this->mailTo[1] = $mailTo[1];
            
        } else {
            $this->mailTo[] = $mailTo;
        }

        
        $this->mail = new PHPMailer();
        $this->config = $config;
    }

    public function useSmtp($value = true)
    {
        $this->isSmtp = $value;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function setMessageBody($message)
    {
        $this->message = $message;
    }

    public function addAddress($emailTo)
    {
        return $this->mailTo[] = $emailTo;
    }

    public function send()
    {
        if ($this->isSmtp)
        {
            $this->mail->isSMTP();                                      // Set mailer to use SMTP
            $this->mail->Host ='ssl://smtp.yandex.ru';             // Specify main and backup SMTP servers
            $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
            $this->mail->Username = 'site@kgbiznes.ru';     // SMTP username
            $this->mail->Password = 'hfccskrb4ST';     // SMTP password
            $this->mail->SMTPSecure ='ssl';                            // Enable TLS encryption, `ssl` also accepted
            $this->mail->Port = 465;        // TCP port to connect to
        }

        $this->mail->setFrom($this->mailFrom, $this->nameFrom);

        foreach ($this->mailTo as $address) {
            $this->mail->addAddress($address);
        }
        
        $this->mail->isHTML($this->config('html', true));
        $this->mail->Subject = $this->subject;
        $this->mail->Body = $this->message;
        $this->mail->CharSet = 'UTF-8';

        return $this->mail->send();
    }

    protected function config($key, $default = null)
    {
        return isset($this->config[$key])
            ? $this->config[$key]
            : $default;
    }

    public function getErrors()
    {
        return $this->mail->ErrorInfo;
    }

    public function getErrorsInline()
    {
        return $this->getErrors();
    }
}