<?
/**
* Simple example of extending the SQLite3 class and changing the __construct
* parameters, then using the open method to initialize the DB.
*/
class MyDB extends SQLite3 {
    function __construct() {
         $this->open('db.db');
    }
}
$db = new MyDB();
if ($_REQUEST['page'] == 'adminRemoveSlide'){
    $id = $_REQUEST['id'];
    $delete =  $db->query("DELETE FROM sliders WHERE id = '".$id."'");
    print_r(json_encode($delete));
}else if ($_REQUEST['page'] == 'adminRemoveGallery'){
    $id = $_REQUEST['id'];
    $delete =  $db->query("DELETE FROM gallery WHERE id = '".$id."'");
    print_r(json_encode($delete));
}else if ($_REQUEST['page'] == 'adminEditCopy'){
    $id = $_REQUEST['copyId'];
    $text = $_REQUEST['copyText'];
    $update =  $db->query("UPDATE textes SET copyrights = '".$text."' WHERE id = '".$id."'");
    print_r(json_encode($_REQUEST));
}else if ($_REQUEST['page'] == 'adminMain'){
    $select =  $db->query("SELECT * FROM main, counters");
    $row['main'] = array(); 
    $i = 0; 
    while($main = $select->fetchArray(SQLITE3_ASSOC)){ 
        $row['main'][$i] = $main; 
        $i++; 
    }
    print (json_encode($row['main'][0]));
}else if ($_REQUEST['page'] == 'adminPassEdit'){
    $user = $_REQUEST['admin'];
    $select =  $db->query("SELECT * FROM passwords WHERE name = '$user'");
    $row['admin'] = array(); 
    $i = 0; 
    while($users = $select->fetchArray(SQLITE3_ASSOC)){ 
        $row['admin'][$i] = $users; 
        $i++; 
    }
    print (json_encode($row['admin'][0]));
}else if ($_REQUEST['page'] == 'adminEditSlide'){
    $id = $_REQUEST['id'];
    $name = $_REQUEST['name'];
    $linkUrl = $_REQUEST['linkUrl'];
    $linkText = $_REQUEST['linkText'];
    $slider = $_REQUEST['slider'];
    $slideTitle = $_REQUEST['slideTitle'];
    $updateImage =  $db->query("UPDATE sliders SET imageText = '".$name."', linkUrl = '".$linkUrl."', sliderName = '".$slider."', buttonText = '".$linkText."', imageTitle = '".$slideTitle."' WHERE id = '".$id."'");
    print_r(json_encode($_REQUEST));
}else if ($_REQUEST['page'] == 'adminEditGallery'){
    $id = $_REQUEST['id'];
    $imageTitle = $_REQUEST['imageTitle'];
    $imageLink = $_REQUEST['imageLink'];
    $galleryName = $_REQUEST['galleryName'];
    $updateImage =  $db->query("UPDATE gallery SET imageTitle = '".$imageTitle."', imageLink = '".$imageLink."', galleryName = '".$galleryName."' WHERE id = '".$id."'");
    print_r(json_encode($_REQUEST));
}else if ($_REQUEST['page'] == 'adminPassUpdate'){
    $id = $_REQUEST['id'];
    $name = $_REQUEST['newName'];
    $pass = $_REQUEST['pass'];
    $updateImage =  $db->query("UPDATE passwords SET name = '".$name."', pass = '".$pass."' WHERE id = '".$id."'");
    print_r(json_encode($name));
}else if ($_REQUEST['page'] == 'adminMainEdit'){
    // main information     
    $id = 1;
    $galleryCountLine = $_REQUEST['galleryCountLine'];
    $menu = $_REQUEST['menuType'];
    $font = $_REQUEST['font'];
    $phone = $_REQUEST['phone'];
    $mail = $_REQUEST['mail'];
    $mailSend = $_REQUEST['mailSend'];
    $mainTitle = $_REQUEST['mainTitle'];
    // yandex counters    
    $yaId = $_REQUEST['yaId'];
    $yaTarget = $_REQUEST['yaTarget'];
    // Update main information
    $updateAdmin =  $db->query("UPDATE main SET galleryCountLine = '".$galleryCountLine."', font = '".$font."', menuType = '".$menu."', mainPhone = '".$phone."', mainMail = '".$mail."', mainMailSend = '".$mailSend."', mainTitle = '".$mainTitle."' WHERE id = '".$id."'");
    // Update yandex counter and send target
    $updateAdminCounter =  $db->query("UPDATE counters SET yaCounterId = '".$yaId."', yaCounterTarget = '".$yaTarget."' WHERE id = '1'");
    print_r(json_encode($_REQUEST));
}else if ($_REQUEST['page'] == 'adminEditTextes'){
    $db1 = new MyDB();
    $data = json_decode(file_get_contents('php://input'), true);
    $text1 = $data['text1'];
    $text2 = $data['text2'];
    $text3 = $data['text3'];
    $text4 = $data['text4'];
    $updateText1 =  $db1->busyTimeout(3000);

    $statement = $db->prepare('UPDATE blocks SET blockContent = :text1 WHERE id = 1');
    $statement->bindValue(':text1', $text1);
    $result = $statement->execute();

    $statement2 = $db->prepare('UPDATE blocks SET blockContent = :text2 WHERE id = 2');
    $statement2->bindValue(':text2', $text2);
    $result2 = $statement2->execute();

    $statement3 = $db->prepare('UPDATE blocks SET blockContent = :text3 WHERE id = 3');
    $statement3->bindValue(':text3', $text3);
    $result3 = $statement3->execute();

    $statement4 = $db->prepare('UPDATE blocks SET blockContent = :text4 WHERE id = 4');
    $statement4->bindValue(':text4', $text4);
    $result4 = $statement4->execute();

    // print_r(json_encode($_POST));
    echo json_encode($result);
}else{

        $passwords = $db->query('SELECT * FROM passwords');
        $row['passwords'] = array(); 
        $i = 0; 
        while($pas = $passwords->fetchArray(SQLITE3_ASSOC)){ 
            $row['passwords'][$i] = $pas; 
            $i++; 
        }
        $slides = $db->query('SELECT * FROM sliders');
        $row['slides'] = array(); 
        $i = 0; 
        while($img = $slides->fetchArray(SQLITE3_ASSOC)){ 
            $row['slides'][$i] = $img; 
            $i++; 
        }
        $copyrights = $db->query('SELECT * FROM textes');
        $row['copyrights'] = array(); 
        $i = 0; 
        while($copy = $copyrights->fetchArray(SQLITE3_ASSOC)){ 
            $row['copyrights'][$i] = $copy; 
            $i++; 
        }
        $blocks = $db->query('SELECT * FROM blocks');
        $row['blocks'] = array(); 
        $i = 0; 
        while($block = $blocks->fetchArray(SQLITE3_ASSOC)){ 
            $row['blocks'][$i] = $block; 
            $i++; 
        }
        $options = $db->query('SELECT * FROM options');
        $row['options'] = array(); 
        $i = 0; 
        while($option = $options->fetchArray(SQLITE3_ASSOC)){ 
            $row['options'][$i] = $option; 
            $i++; 
        }
        $gallery = $db->query('SELECT * FROM gallery');
        $row['gallery'] = array(); 
        $i = 0; 
        while($galleryI = $gallery->fetchArray(SQLITE3_ASSOC)){ 
            $row['gallery'][$i] = $galleryI; 
            $i++; 
        }
        print (json_encode($row));
        // print_r($result->fetchArray(SQLITE3_ASSOC));
        // print(json_encode($result));
}
?>
