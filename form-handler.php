<?php
require_once __DIR__ . '/mailer/emails/header.php';
require_once __DIR__ . '/mailer/emails/footer.php';
require __DIR__ . '/mailer/vendor/autoload.php';
date_default_timezone_set('Europe/Moscow');

$post = json_decode(file_get_contents('php://input'), true);
$post['MAIL_HEADER'] = $mailHeader;
$post['MAIL_FOOTER'] = $mailFooter;
$post['BLOCK_DATE'] = $blockDate;

$siteName = 'Тест админки';
$mailFrom = ['admin@admin.ru', $siteName];
$currentDay = date("d.m.y");
$currentTime = date("H:i");
$title='Заявка с сайта ' .$siteName;
$siteUrl = $_SERVER['HTTP_HOST'];

$post['currentDay'] = $currentDay;
$post['currentTime'] = $currentTime;
$post['title'] = $title;
$post['siteName'] = $siteName;
$post['siteUrl'] = $siteUrl;

$formId = 'callbackForm';
use DigitalHammer\LpForms\Form;
use DigitalHammer\LpForms\Mailer;
use DigitalHammer\LpForms\FormHandler;
/**
 * Settings
 */
$mailTo[0] = $post['mailSend'];
$fieldNames = [
    'name' => 'Ваше имя',
    'phone' => 'Ваш телефон',
    'mail' => 'Ваш E-mail',
];
$name  = $post['name'];
$phone = $post['phone'];
$mail  = $post['mail'];
/**
 * Mailer
 */
$mailer = new Mailer($mailFrom, $mailTo);
$mailer->setSubject($title);
/**
 * callbackForm
 */
$callbackForm = new Form('callbackForm', $post, $mailer);
$callbackForm
    ->addField('phone', [])
    ->addField('name', [])
    ->addField('mail', [])
    ->addField('date', [])
    ->addField('time', [])
    ->addField('MAIL_HEADER', [])
    ->addField('MAIL_FOOTER', [])
    ->addField('currentDay', [])
    ->addField('currentTime', [])
    ->addField('siteName', [])
    ->addField('siteUrl', [])
    ->addField('title', [])
    ->addField('formName', [])
    ->setFieldNames($fieldNames)
    ->setMessageBodyTemplate(__DIR__ . '/mailer/emails/callback');
    
$formHandler = new FormHandler();
$formHandler->addForm($callbackForm);
try {
    $formHandler->handle($formId);

} catch (Exception $exception) {
    (new \DigitalHammer\LpForms\ResponseJson())->fail(
        $exception->getMessage()
    );
}
