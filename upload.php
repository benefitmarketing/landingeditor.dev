<?php
// $_FILES = ['uploads/2017-08-25-13-19-59.jpg'];
// $_FILES[ 'file' ][ 'tmp_name' ] = 'uploads/2017-08-25-13-19-59.jpg';
if ( !empty( $_FILES ) ) {
class MyDB extends SQLite3 {
	function __construct() {
		 $this->open('config/db.db');
	}
}
include 'mailer/PHPThumb/PHPThumb.php';
include 'mailer/PHPThumb/PluginInterface.php';
include 'mailer/PHPThumb/GD.php';
$options = array('resizeUp' => true, 'jpegQuality' => 80);
$db = new MyDB();

    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
    $uploadPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];
    $tempType = explode('.',$_FILES[ 'file' ][ 'name' ]);
    $tempType = end($tempType);
    $tempName = date("Y-m-d-H-i-s").".".$tempType;
    $fileUrl = 'uploads/'.$_FILES[ 'file' ][ 'name' ];
    $bigFile = 'uploads/'.$tempName;
    $smallFile = 'uploads/small/'.$tempName;
    $thumb = new PHPThumb\GD($tempPath);
    $thumb->setFileName ($tempName);
    $thumb->save($bigFile);
    $thumb->resize(100, 100);
    $thumb->save($smallFile);
    if ($_REQUEST['type'] == 'gallery'){
        $insert = "INSERT INTO 'gallery' (id, galleryName, imageUrl, minImageUrl, imageTitle, imageLink) VALUES (NULL, NULL, '$bigFile', '$smallFile', '$tempName', NULL )";
        $statement = $db->query($insert);
    }else{
        $insert = "INSERT INTO 'sliders' (imageUrl, imageText, linkUrl, id, sliderName,minImageUrl, imageTitle) VALUES ('$bigFile','$tempName','',NULL, NULL,'$smallFile', NULL)";
        $statement = $db->query($insert);
    }
    $answer = array( 'answer' => $statement );
    // $answer = $_FILES;
    $json = json_encode( $answer );
    echo $json;
} else {
    echo 'No files';
}
?>